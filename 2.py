# Завдання 2
# Створіть три функції, одна з яких читає файл на диску із заданим ім'ям та перевіряє наявність рядка «Wow!».
# Якщо файлу немає, то засипає на 5 секунд, а потім знову продовжує пошук по файлу. Якщо файл є, то відкриває його
# і шукає рядок «Wow!». За наявності цього рядка закриває файл і генерує подію, а інша функція чекає на цю подію і
# у разі її виникнення виконує видалення цього файлу. Якщо рядки «Wow!» не було знайдено у файлі, то засипати на 5
# секунд. Створіть файл руками та перевірте виконання програми.

import os
import threading
import time

# Event for signalization about the found of the row "Wow!"
wow_event = threading.Event()


def search_wow(filename):
    while not wow_event.is_set():
        try:
            with open(filename, "r", encoding="utf-8") as file:
                content = file.read()
                if "Wow!" in content:
                    print(f"Wow! found in {filename}")
                    wow_event.set()
        except FileNotFoundError:
            print(f"File {filename} not found. Sleeping for 5 seconds.")
            time.sleep(5)


def delete_file_on_wow(filename):
    # Wait for the signal from search_wow
    wow_event.wait()
    print(f"Deleting {filename}...")
    try:
        os.remove(filename)
        print(f"{filename} deleted.")
    except FileNotFoundError:
        print(f"File {filename} already deleted.")


if __name__ == "__main__":
    filename = "Text.txt"

    # Create and run threads:
    search_thread = threading.Thread(target=search_wow, args=(filename,))
    delete_thread = threading.Thread(target=delete_file_on_wow, args=(filename,))

    search_thread.start()
    delete_thread.start()

    # waiting for the finish of Threads
    search_thread.join()
    delete_thread.join()

    print("All threads finished")
