# Завдання 1
# Створіть функцію для обчислення факторіала числа. Запустіть декілька завдань, використовуючи Thread,
# і заміряйте швидкість їхнього виконання, а потім заміряйте швидкість обчислення, використовуючи той же набір завдань
# на ThreadPoolExecutor. Як приклади використовуйте останні значення, від мінімальних і до максимально можливих,
# щоб побачити приріст або втрату продуктивності.

import time
import threading
from concurrent.futures import ThreadPoolExecutor


def factorial(n):
    if n == 0 or n == 1:
        return 1
    else:
        return n * factorial(n - 1)


def calculate_factorial(n, method):
    start_time = time.time()
    if method == "thread":
        t = threading.Thread(target=factorial, args=(n,))
        t.start()
        t.join()
    elif method == "thread_pool":
        with ThreadPoolExecutor() as executor:
            future = executor.submit(factorial, n)
            future.result()
    end_time = time.time()
    return end_time - start_time


numbers_factorial = [300, 400, 500, 600]

for num in numbers_factorial:
    time_thread = calculate_factorial(num, "thread")
    time_thread_pool = calculate_factorial(num, "thread_pool")

    print(f"Number: {num}")
    print(f"Thread Time: {time_thread:.6f} seconds")
    print(f"ThreadPool Time: {time_thread_pool:.6f} seconds")
    print("=" * 30)
